import Layout from '../components/Layout'
import withSession from '../lib/session'
import PropTypes from 'prop-types'
const SsrProfile = ({ user }) => {
  return (
    <Layout>
      <h1>Signed in as {user.upn}</h1>
      {user?.isLoggedIn && (
        <>
          <p>Hello, {user.name}</p>
          <p>Testing merge</p>
          <p>Testing merge</p>
          <p>Testing merge</p>
          <p>Testing merge</p>
          <p>Testing merge</p>
          <p>Testing merge</p>
          <p>Testing merge</p>
          <p>Testing merge</p>
          <p>Testing merge</p>
          {/* <pre>{JSON.stringify(user, null, 2)}</pre> */}
        </>
      )}
    </Layout>
  )
}
export const getServerSideProps = withSession(async function ({ req, res }) {
  const user = req.session.get('user')
  if (!user) {
    return {
      redirect: {
        destination: '/login',
        permanent: false,
      },
    }
  }
  return {
    props: { user: req.session.get('user') },
  }
})
export default SsrProfile
SsrProfile.propTypes = {
  user: PropTypes.shape({
    isLoggedIn: PropTypes.bool,
    upn: PropTypes.string,
    profilePicturePath: PropTypes.string,
  }),
}
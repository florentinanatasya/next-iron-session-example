import Layout from '../components/Layout'

const Home = () => (
  <Layout>
    <h1>
      Next-Iron-Session-Authentication example
    </h1>
    <style jsx>{`
      li {
        margin-bottom: 0.5rem;
      }
    `}</style>
  </Layout>
)

export default Home
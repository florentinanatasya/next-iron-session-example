import withSession from '../../lib/session'
import { onLogin } from '../../helper'

export default withSession(async (req, res) => {
  let userData ={}
  const { username, password } = await req.body
  const loginData = {
    username: username,
    password: password,
  };
  try {
    return onLogin(loginData).then(async(data) => {
      userData=data
      if (!userData.message){
        const user ={ isLoggedIn: true, ...userData}
        req.session.set('user', user)
        await req.session.save()
        res.json(user)
      }
      else{
        const error = new Error(userData.message)
        throw error
      }
    })
  } catch (error) {
    const { response: fetchResponse } = error
    res.status(fetchResponse?.status || 500).json(error.data)
  }
})
import useUser from '../lib/useUser'
import Layout from '../components/Layout'

const SgProfile = () => {
  const { user } = useUser({ redirectTo: '/login' })

  if (!user || user.isLoggedIn === false) {
    return <Layout>loading...</Layout>
  }

  return (
    <Layout>
      <h1>Signed in as {user.upn}</h1>
      <p>Hello, {user.name}</p>
      <p>Testing merge</p>
      <p>Testing merge</p>
      <p>Testing merge</p>
      <p>Testing merge</p>
      <p>Testing merge</p>
      {/* <pre>{JSON.stringify(user, null, 2)}</pre> */}
    </Layout>
  )
}
export default SgProfile
